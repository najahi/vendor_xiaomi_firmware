# Supported device
SUPPORTED_DEVICE := willow ginkgo

# Test
TARGET_DEVICE := $(CUSTOM_BUILD)
ifeq ($(TARGET_DEVICE),)
  ifeq ($(PRODUCT_DEVICE),)
    ifeq ($(CUSTOM_BUILD),)
      ifeq ($(target),)
        $(error "firmware: Cannot identify the device, aborting build")
      else
        DEVICE := $(target)
      endif
    else
      DEVICE := $(CUSTOM_BUILD)
    endif
  else
    DEVICE := $(PRODUCT_DEVICE)
  endif
else
  DEVICE := $(TARGET_DEVICE)
endif

# Include the firmware if DEVICE matches with SUPPORTED_DEVICE
ifneq ($(filter $(SUPPORTED_DEVICE),$(DEVICE)),)
  PRODUCT_COPY_FILES := $(call find-copy-subdir-files,*,$(LOCAL_PATH)/$(DEVICE)/,install/firmware-update)
else
  $(warning "firmware: DEVICE has no supported firmware")
endif

